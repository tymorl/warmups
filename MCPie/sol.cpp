#include<iostream>
#include<random>

class Point {
	public:
		Point(long double ix, long double iy) {
			x = ix;
			y = iy;
		}
		bool inCircle() {
			return x*x + y*y < 1.;
		}
	private:
		long double x, y;
};

class PieEstimator {
	public:
		PieEstimator(long double prec) {
			std::random_device rd;
			gen = std::mt19937(rd());
			dis = std::uniform_real_distribution<long double>(-1.0, 1.0);
			precision = prec;
			total = 1;
			inCircle = 0;
		}
		long double bake() {
			generatePoints();
			return estimate();
		}
	private:
		long double precision;
		std::mt19937 gen;
		std::uniform_real_distribution<long double> dis;
		long double total, inCircle;
		void generatePoints() {
			for (long double i = 0.; i < 1./(precision*precision); i+=1.) {
				generatePoint();
			}
		}
		void generatePoint() {
			long double x = dis(gen), y = dis(gen);
			total += 1.;
			if (Point(x, y).inCircle()) {
				inCircle += 1.;
			}
		}
		long double estimate() {
			return 4.*inCircle/total;
		}
};

int main() {
	auto oven = PieEstimator(0.001);
	std::cout << oven.bake() << std::endl;
	return 0;
}
