#include<iostream>
#include<algorithm>

class NonAdjecentSumFinder {
	public:
		NonAdjecentSumFinder() {
			current = 0;
			previous = 0;
		}
		void add(int k) {
			int oldCurr = current;
			current = std::max(previous + k, oldCurr);
			previous = oldCurr;
		}
		int max() {
			return current;
		}
	private:
	 int previous, current;
};

int main() {
	int n, k;
	std::cin >> n;
	auto asf = NonAdjecentSumFinder();
	for (int i = 0; i < n; i++) {
		std::cin >> k;
		asf.add(k);
	}
	std::cout << asf.max() << std::endl;
	return 0;
}
