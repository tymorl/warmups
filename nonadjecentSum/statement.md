# Statement

Given a list of integers, write a function that returns the largest sum of non-adjacent numbers. Numbers can be 0 or negative.

For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5. [5, 1, 1, 5] should return 10, since we pick 5 and 5.

Follow-up: Can you do this in O(N) time and constant space?

# Input specification

An integer $n$, followed by a newline. Then $n$ integers separated by spaces, followedd by a newline.

It would be anough to figure out a delimiter and then the amount of integers can be arbitrary, but this would make the scanning annoying.

#Source
@Absinthe@qoto.org, https://qoto.org/@Absinthe/102912078174860674
