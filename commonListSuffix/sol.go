package main

// I'd love to use the standard library list (not really, because `interface{}`, blah), but one cannot do this with it.
import (
	"fmt"
)

// Problem setup.

type List struct {
	next *List
	Value int
}

func Cons(k int, l *List) *List {
	return &List{
		next: l,
		Value: k,
	}
}

func (l *List) Next() *List {
	return l.next
}

func common() *List {
	commonSuffix := &List{
		Value: 10,
	}
	commonSuffix = Cons(8, commonSuffix)
	return commonSuffix
}

func example() (*List, *List) {
	commonSuffix := common()
	A := Cons(3, Cons(7, commonSuffix))
	B := Cons(99, Cons(1, commonSuffix))
	return A, B
}

func otherTest() (*List, *List) {
	commonSuffix := common()
	A := Cons(3, Cons(7, commonSuffix))
	B := Cons(2137, Cons(43, Cons(99, Cons(1, commonSuffix))))
	return A, B
}

// Actual solution.

func CommonSuffix(A, B *List) *List {
	var lenA, lenB int
	for l := A; l != nil; l = l.Next() {
		lenA++
	}
	for l := B; l != nil; l = l.Next() {
		lenB++
	}
	if lenA < lenB {
		bufL := A
		A = B
		B = bufL
		bufI := lenA
		lenA = lenB
		lenB = bufI
	}
	for toCut := lenA - lenB; toCut > 0; toCut-- {
		A = A.Next()
	}
	for A != B {
		A = A.Next()
		B = B.Next()
	}
	return A
}

func main() {
	fmt.Printf("%d\n", CommonSuffix(example()).Value)
	fmt.Printf("%d\n", CommonSuffix(otherTest()).Value)
}
